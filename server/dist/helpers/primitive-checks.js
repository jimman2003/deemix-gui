"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isObjectEmpy = void 0;
const isObjectEmpy = (obj) => Object.keys(obj).length === 0;
exports.isObjectEmpy = isObjectEmpy;
